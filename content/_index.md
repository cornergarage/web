+++
+++

{{< hero caption="Photo by [ikeofspain](https://www.flickr.com/photos/ikeofspain/31144130658/lightbox)"
         height="73vh"
         img="/images/lambo.jpg"
         mailchimp=true >}}
## Our Garage is Your Garage

Take control of your auto repairs and maintenance by doing it yourself at
**Corner Garage**!  
Whether you're, prepping for race season, changing your oil, or restoring a
classic we've got the tools and space to get the job done.

Sign up for our mailing list to be the first to know when we open our doors:

{{< /hero >}}


## Hours

We're not quite ready to open the doors yet, but feel free to sign up for our
[mailing list](#our-garage-is-your-garage) and we'll let you know when we open!

Tentatively, our hours will be:

|             |                              |
|:------------|:-----------------------------|
| **Mon**     | [by appointment][contact us] |
| **Tue–Fri** | 11am–10pm                    |
| **Sat–Sun** | 8am–10pm                     |

All bays are available on a first-come-first-serve basis.
For after hours or early morning access, [contact us] to schedule an
appointment.


## Services

When **Corner Garage** opens we will provide the following services:

{{< flex justify-content="space-between" >}}

{{< wrap icon="wrench" title="Workspace" >}}
Bays and workbenches are billed in 15 min increments and the price includes a
fully stocked mechanics toolbox.
Basic air tools and lesser used tools are available at the front desk free of
charge.

Lift Bay
: $40/hr

Flat Bay
: $20/hr

Workbench
: $10/hr
{{< /wrap >}}

{{< wrap icon="car" title="Storage" >}}
Vehicle storage is available for those times when you don't quite finish a
project or need someone to occasionally run your engine while you're out of
town.
Lockers are also available for equipment storage.

Indoor
: $20/day

Outdoor
: $10/day

Locker
: $5/day
{{< /wrap >}}

{{< wrap icon="tree" title="Oil Recycling" >}}
We can recycle oil, coolant, and oil filters free of charge.
No purchase necessary.
Ask about receiving an oil change discount and avoid spilling oil in your drive
way or parking lot!

Recycling
: Free
{{< /wrap >}}

{{< /flex >}}

If you're working on a restoration project, or if you're a professional mechanic
looking for a space to work from, [contact us] to discuss long-term rates.

{{< hero caption="Photo by [ikeofspain](https://www.flickr.com/photos/ikeofspain/31144121528/lightbox)"
         height="70vh"
         img="/images/atom.jpg"
         member=true >}}
## Membership

**Corner Garage Co-op** is customer owned co-operative.
Having our direction and management democratically controlled by the community
is our way of helping the Atlanta area build its social infrastructure.
You shouldn't have to be rich to own part of a business in your community.
We want anyone to be able to walk through our doors and become a customer, or
even an owner.

If you want to get involved with **Corner Garage**, or just like to support
local businesses and fair labor policies, consider becoming a member.
{{< /hero >}}


## Events

{{< events >}}


## Blog

{{< blog >}}

## Contact

To contact us fill out the form below or use one of the other mechanisms listed
here.

{{< flex justify-content="space-between" >}}

<form name="contact" method="POST" netlify-honeypot="hide" data-netlify="true">
  <p class="hidden"><label>Don’t fill this out if you're human: <input name="hide"/></label></p>
  <label for="contactname">Your Name:</label>
  <input type="text" name="name" id="contactname" placeholder="Rubens Barrichello"/>
  <label for="contactemail" >Your Email:</label>
  <input type="email" name="email" id="contactemail" placeholder="rubens@example.net"/>
  <label for="contactmessage">Message:</label>
  <textarea name="message" id="contactmessage"></textarea>
  <button type="submit" class="primary">Send</button>
</form>

<dl>
<dt>Email</dt>
<dd>{{% email %}}</dd>

<dt>Social</dt>
<dd>{{% icons %}}</dd>

<dt>Location</dt>
<dd>
We're still looking for a location, but we'll let you know soon! {{% map %}}
</dd>
</dl>

{{< /flex >}}

[contact us]: #contact
