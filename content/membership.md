+++
title = "Membership"
+++

{{< hero caption="Photo by [WillVision Photography](https://www.flickr.com/photos/willvision/21266038616/lightbox)"
         height="73vh"
         img="/images/prelude.jpg"
         mailchimp=true >}}
## Membership

**Corner Garage** is customer owned and employee managed co-operative.
Having our direction and management democratically controlled by the community
is our way of helping the Atlanta area build its social infrastructure.
You shouldn't have to be rich to own part of a business in your community.
We want anyone to be able to walk through our doors and become a customer, or
even an owner.
If you want to get involved with **Corner Garage**, or just like to support
local businesses and fair labor policies, consider becoming a member.

Sign up for our mailing list to be notified when you can become a member!

{{< /hero >}}

## Benefits

Membership benefits include a discount in the shop, the right to run for or vote
on the Board of Directors, and access to members-only deals and events.
You'll also get a say in how we run things including the ability to help shape
the draft bylaws and policies below.

{{< flex justify-content="space-between" >}}

{{< wrap >}}
### Certificate of Formation

**Corner Garage** will be a co-operative organization.
A draft of the Certificate of Formation that will be filed with the Georgia
Secretary of State can be found [here](/docs/cert.pdf)&nbsp;(PDF).
{{< /wrap >}}

{{< wrap >}}
### Bylaws

The co-op will be bound by a set of bylaws that will be voted on by the members.
A draft of the bylaws which will be proposed and voted on at the first member
meeting can be found [here](/docs/bylaws.pdf)&nbsp;(PDF).
{{< /wrap >}}

{{< wrap >}}
### Policies

The Board of Directors and General Manager run the business using a set of
written policies.
All board and GM policies are publicly available and can be found in the [Policy
Registry](/docs/policies.pdf)&nbsp;(PDF).
{{< /wrap >}}

{{< /flex >}}
