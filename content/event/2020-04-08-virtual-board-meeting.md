+++
title       = "Virtual Board Meeting"
description = ""
date        = 2020-03-17T09:03:56-04:00
start       = 2020-04-08T18:00:00-04:00
end         = 2020-04-08T19:00:00-04:00
location    = "https://meet.jit.si/cornergarageboardmeeting"
authors     = ["Sam Whited <sam@cornergarage.coop>"]
tags        = ["Board Meeting"]
+++

The first board meeting held since incorporating in Georgia will be held by
video conference due to the shelter in place order.

<!--more-->

Anyone is welcome to attend.

Join: https://meet.jit.si/cornergarageboardmeeting

To join by phone instead, dial: [+1.512.402.2718,,875878668#](tel:+1.512.402.2718,,875878668#)

Looking for a different dial-in number?  
See meeting dial-in numbers:
https://meet.jit.si/static/dialInInfo.html?room=cornergarageboardmeeting
