+++
title       = "Cars & Coffee"
description = "Corner Garage will be photographing Cars & Coffee at CoTA."
date        = 2019-05-12T13:11:35-05:00
start       = 2019-05-19T08:00:00-05:00
end         = 2019-05-19T12:00:00-05:00
location    = "Circuit of The Americas 9201 Circuit of the Americas Blvd, Austin, Texas 78617"
authors     = ["Sam Whited <sam@cornergarage.coop>"]
tags        = ["Cars & Coffee"]
+++

A Corner Garage board member will be at Austin [Cars & Coffee] photographing
cars and answering questions about our upcoming opening.
If you'd like free photos of your car, or want to know more about us, [drop us a
line] and let's meet up!

[Cars & Coffee]: http://www.circuitoftheamericas.com/cars-and-coffee
[drop us a line]: /#contact
