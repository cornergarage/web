+++
title       = "Board Meeting"
description = ""
date        = 2019-07-13T09:20:26-05:00
start       = 2019-07-17T15:00:00-05:00
end         = 2019-07-17T16:00:00-05:00
location    = "Sa-Ten Coffee & Eats 4917 Airport Blvd, Austin, TX 78751"
authors     = ["Sam Whited <sam@cornergarage.coop>"]
tags        = ["Board Meeting"]
+++

The first Board meeting will be held at Sa-Ten Coffee & Eats on Airport Blvd.

The agenda is as follows:

- Board introductions
- Agree on a format for meetings
- Elect a president of the board
- Review and vote on incorporating documents
- Listen to community proposals (if any)
