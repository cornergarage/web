+++
title    = "Member Info"
noindex  = true
nofollow = true
+++

## Welcome!

Thank you for purchasing a Corner Garage membership!
Before you can vote in any upcoming elections, or receive your member discount
you'll need a membership card.

Please provide an address so we can send you your card, as well as an email so
that you can receive notice of member meetings, important votes, or new member
benefits.
Use the same name and email you used to check out, if possible.
This lets us verify your purchase without having to contact you.

<form name="contact" method="POST" netlify-honeypot="hide" data-netlify="true">
  <p class="hidden"><label>Don’t fill this out if you're human: <input name="hide"/></label></p>
  <label for="contactname">Your Name:</label>
  <input type="text" name="name" id="contactname" placeholder="Sebastian Vettel"/>
  <label for="contactemail" >Your Email:</label>
  <input type="email" name="email" id="contactemail" placeholder="sebastian@example.net"/>
  <label for="contactaddr">Mailing Address:</label>
  <textarea name="addr" id="contactaddr" placeholder="123 Fake Street&#10;Atlanta, GA 30313"></textarea>
  <button type="submit" class="primary">Submit</button>
</form>
