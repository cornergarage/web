+++
title   = "Incorporation"
date    = "2020-01-11T09:54:06-05:00"
noindex = false
+++

Now that it's the 20s again, we're sure you're looking for somewhere to restore
that rusted out Ford Model T or Bentley Speed Six that your great grandparents
once drove, and we've got good news on that front: Corner Garage has just filed
paperwork to incorporate in the state of Georgia!

While this doesn't mean we'll be opening any time soon, it does mean that we'll
be able to kick off a funding drive and start accepting memberships.
Keep your ears to the ground, we'll have more on that front very soon.

In the mean time, if anyone *does* have a Bently Speed Six Blue Train gathering
dust in their garage, are you interested in selling?

