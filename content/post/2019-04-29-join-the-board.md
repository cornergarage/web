+++
title       = "Join the Board"
description = "A call for participants to stand for the Board of Directors."
date        = "2019-04-29T00:00:00-05:00"
authors     = ["Sam Whited"]
tags        = ["Membership", "Board"]
+++

One of the last things Corner Garage needs before we can start seeking funding
is a dedicated board of directors to see us through our formative period.
If you're a Texas resident who's passionate about cars, car repair, or even just
the cooperative business model, consider running for the board of directors.

<!--more-->

### What is the board?

The board of directors is the governing body of a co-op.
It comprises 5 members, one of whom serves as the president and another as the
treasurer and secretary.
Board members must be member-owners of the co-op themselves, and are elected by
the other members.
In our case board members serve 3 year terms, though they are free to step down
at any time.

### What does the board do?

The board is accountable to the owners and responsible for book keeping,
recruiting new members, and for making the business decisions that keep the
co-op running.
Since we're looking for an initial board of directors, they will be appointed
instead of elected and their only real duty will be to recruit members and
organize an election.
They are free to stand in the election to keep their seats, or step down to make
way for other members that may want to participate.
Aside from that, the board is welcome to be as involved or uninvolved as their
time and commitment allows.
The board may help us search for funding, find a location, or renovate the
space once we have it.

The board is a volunteer position, so a good candidate for the board doesn't
need prior business experience.
Instead, they need to be passionate about the idea of Corner Garage and about
serving the members.
If enough funding is secured, some of the board could even become the first
employees!

### How do I join up?

If you're interested in joining the founding board, [contact us] and we'll get a
coffee and chat.
We can walk you through what we're working on, where we plan on going with the
business, and see if we both think it's a good fit.


[contact us]: /#contact
