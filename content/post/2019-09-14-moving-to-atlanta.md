+++
title       = "Moving to Atlanta"
description = "Corner Garage is moving!"
date        = "2019-09-14T07:17:02-05:00"
authors     = ["Sam Whited"]
tags        = ["Location"]
+++

Corner Garage is hitting the road!
These last few months we've been hard at work putting together a board,
designing print materials, and pitching Corner Garage to anyone who would
listen but we've decided it's time to make a change: Corner Garage will no
longer be opening in Austin.
Instead, we're uprooting once again and going back to the city of Atlanta, GA!

<!--more-->

Atlanta has a thriving Co-op scene that includes everything from [bike][bike]
[shops][shops] to [grocery][grocery] [stores][stores], and soon an automotive
garage. We're excited to help expand Georgia's co-op economy!
We're not yet sure exactly where we'll open Corner Garage Co-op yet, but we're
looking at locations ranging from Atlanta proper to Decatur to East Point.
If you'd like to join us in our search, please reach out or subscribe to our
[mailing list] for future updates!


[mailing list]: /membership/
[bike]: https://bearingsbikeshop.org/
[shops]: http://www.sopobikes.org/
[grocery]: http://www.sevananda.coop/
[stores]: https://market166.com/
