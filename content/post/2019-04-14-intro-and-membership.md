+++
title       = "Introduction and Membership"
description = "An introduction to Corner Garage—a DIY shop in Austin, TX—and the benefits of membership in the co-op."
date        = "2019-04-14T12:24:00-05:00"
authors     = ["Sam Whited"]
tags        = ["Membership"]
+++

Welcome to the **Corner Garage** blog!

We're a DIY automotive repair shop that's planning on opening in
South Austin.
Because we've been formed as a cooperative, we're owned and accountable to you,
our member-owners.
By becoming a member you can help us open our doors, but also get a lot of great
benefits for yourself.

<!--more-->

Here are just a few:

* Vote on our board of directors, policies, and bylaws
* Join the board or a committee and help run the business
* Get paid dividends if we have a profitable month
* Get discounts on bay rental, car storage, and other services
* Get access to our members-only hangout spot

We're gearing up to start accepting memberships right now, and will post more
information to this space when we're ready for you.
If you'd like to be informed when you can become a member-owner of **Corner
Garage**, sign up for the mailing list below (don't worry, we'll keep the emails
to a minimum).


<div id="mc_embed_signup">
<form action="https://cornergarage.us20.list-manage.com/subscribe/post?u=cf1fbbce5ec6583d46c487a0c&amp;id=c0511d99a3" method="post" id="mc-embedded-subscribe-form" name="mc-embedded-subscribe-form" class="validate" target="_blank" novalidate>
<div id="mc_embed_signup_scroll">

<div class="mc-field-group">
<label for="mce-EMAIL">Email Address </label>
<input type="email" value="" name="EMAIL" class="required email" id="mce-EMAIL"/>
</div>
<div id="mce-responses" class="clear">
<div class="response" id="mce-error-response" style="display:none"></div>
<div class="response" id="mce-success-response" style="display:none"></div>
</div>    <!-- real people should not fill this in and expect good things - do not remove this or risk form bot signups-->
<div style="position: absolute; left: -5000px;" aria-hidden="true"><input type="text" name="b_cf1fbbce5ec6583d46c487a0c_c0511d99a3" tabindex="-1" value=""></div>
<div class="clear"><input type="submit" value="Subscribe" name="subscribe" id="mc-embedded-subscribe" class="button primary"/></div>
</div>
</form>
</div>

If you're interested in investing in **Corner Garage**, or have any questions
about us, please [contact us](/#contact).
