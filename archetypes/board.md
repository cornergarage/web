+++
title       = "{{ dateFormat "2006-01-02" .Date }}"
date        = "{{ .Date }}"
authors     = []
draft       = true
+++

## Agenda

- Roll Call
- Agenda Bashing
- Old Business
- New Business
- Date of Next
- Any Other Business (AOB)
- Close

## Minutes

To Do
