var stripe = Stripe("{{- if .Site.IsServer -}}
{{- .Site.Params.stripe.stripe_test_key -}}
{{- else -}}
{{- .Site.Params.stripe.stripe_live_key -}}
{{- end -}}");

{{- range .Site.Params.stripe.products }}
{
let checkoutButton = document.getElementById('checkout-button-{{ .id }}');
checkoutButton.addEventListener('click', function () {
  stripe.redirectToCheckout({
    items: [{{- if .Site.IsServer -}}
{sku: '{{ .test_sku }}', quantity: 1}
{{- else -}}
{sku: '{{ .live_sku }}', quantity: 1}
{{- end }}],
    successUrl: 'https://cornergarage.coop/memberinfo',
    cancelUrl: 'https://cornergarage.coop/canceled',
  })
  .then(function (result) {
    if (result.error) {
      var displayError = document.getElementById('error-message');
      displayError.textContent = result.error.message;
    }
  });
});
}
{{- end }}
