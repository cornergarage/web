# Calendar


**Calendar** is a [Hugo] theme component that generates a calendar view for
pages in the `event` category.


[Hugo]: https://gohugo.io/


## Creating Events

An events archetype is included so that creating events is easy: `hugo new
event/myevent.md`

This will create a file with all the supported metadata set to default values.


## List Page

The default list page for the event section renders a calendar, but you probably
want to integrate it with your existing theme.
To do so, create a file called `layouts/event/list.html` and load the
`calendar.html` partial in it.
For example, if your primary theme defines a section called `main` which should
contain page contents, you could define a template like this to render your
calendar:

```
{{ define "main" }}
<main role="main">
	{{- .Content }}
	{{- partial "calendar.html" . }}
</main>
{{ end }}
```

You will also need to render and include the asset `calendar.css` in your head,
and a meta element pointing to the calendar for discovery.
This can be done by including the partial `calendarmeta.html` in your head:

```
{{- partial "calendarmeta.html" . }}
```

This will check if the page is an event page and if the calendar output is
enabled, and if so it will render something like the following:

```
<link href="webcal://localhost:1313/events/index.ics"
			rel="alternate"
			type="text/calendar"
			title="Your Site"/>
<link rel="stylesheet"
			href="http://localhost:1313/css/calendar.min.1bc0eddbfba8c3032a6997e245f7049494a84288e9be2756c549c4febd17b2a9.css"
			integrity="sha256-G8Dt2/uowwMqaZfiRfcElJSoQojpvidWxUnE/r0Xsqk="/>
```


## Configuration

To configure the component, add it to your themes list and output types.


```toml
theme           = ["calendar", "mytheme"]

[outputs]
	section = ["HTML", "Atom", "Calendar"]
	page    = ["HTML", "Calendar"]
```

You can also set the following properties in your `config.toml` to configure the
calendar:

**style.accentPrimary:** The primary accent color (defaults to `#81a1c1`).

**style.accentSecondary:** The secondary accent color used in the info bar (defaults to `#a3be8c`).

**style.backgroundDark:** A dark background and text color (defaults to `#232432`).

**style.backgroundLight:** A light background and text color (defaults to `#fcfcfc`).


## Submitting Patches

For information about development and submitting patches to this theme, see
[`CONTRIBUTING.md`].

[`CONTRIBUTING.md`]: https://git.sr.ht/~samwhited/calendar/tree/master/CONTRIBUTING.md


## License

Licensed under the BSD 2 Clause License ([LICENSE] or
https://opensource.org/licenses/BSD-2-Clause)

[LICENSE]: https://git.sr.ht/~samwhited/calendar/tree/master/LICENSE


### Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you shall be licensed as above, without any
additional terms or conditions.
